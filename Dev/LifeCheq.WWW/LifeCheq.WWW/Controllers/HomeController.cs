﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LifeCheq.WWW.Models;

namespace LifeCheq.WWW.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Calculate(BasicCalculation basicCalculation)
        {
            double rate = basicCalculation.fixedInterestRate / 100 / 12;

            double denaminator = Math.Pow((1 + rate), basicCalculation.bondTermInYears * 12) - 1;

            var result = (rate + (rate / denaminator)) * (basicCalculation.purchasePrice - basicCalculation.depositPaid);

            CalculateOVM ovm = new CalculateOVM
            {
                bondTermInYears = basicCalculation.bondTermInYears,
                depositPaid = basicCalculation.depositPaid,
                fixedInterestRate = basicCalculation.fixedInterestRate,
                purchasePrice = basicCalculation.purchasePrice,
                CalculationResult = result
            };

            return View(ovm);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
