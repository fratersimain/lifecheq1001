﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LifeCheq.WWW.Models
{
    public class CalculateOVM
    {
        public double purchasePrice { get; set; }

        public double depositPaid { get; set; }

        public double bondTermInYears { get; set; }

        public double fixedInterestRate { get; set; }

        public double CalculationResult { get; set; }
    }
}
